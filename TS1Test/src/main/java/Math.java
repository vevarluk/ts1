
public class Math {


    public long factorial(int n){
        long ret = n;
        for(int i=1; i<(n-1); i++){
            ret=ret*(n-i);
        }
        return ret;
    }
    
    public long factorial2(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial2(n - 1);
        }
    }

}
